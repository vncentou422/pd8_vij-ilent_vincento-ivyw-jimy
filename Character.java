/*
   Le Character skeleton
   Character is the forefather to the Enemy and Player classes.
   All things that will actually do work in terms of the game stem from this class
*/

public class Character{
    //le variables
    protected String name;
    protected int _HP;
    protected int attack;
    protected int defense;

    private boolean _defending;
    //end le variables

    public Character(){
        _HP = 100;
        attack = 20;
        defense = 20;
        _defending = false;
    }

    //el damage calculator
    public int calc(int num){

        int dmg = (int)(Math.random() * 15) + num;
        if(Math.random() <= 0.2){
            dmg =  dmg * 2;
        }
        return dmg + attack;
    }

    public String getName(){
        return name;
    }

    //with the attack method, the character launches an attack

    //initial damage is calculated
    //the potential critical chance is factored in
    //damge is altered based on whether the enemy has defense up
    //the target then loses hp equal to that damage
    public int attack(Character target){
        int y = 0;
        if (!target.isDef()){
            y = calc((int)Math.random()*30);
            target.setHP(y);
        }
        else{
            target.setDefend(false);
            y = (int)(calc((int)Math.random()*30)/(defense/5.0));
            target.setHP(y);
        }
        System.out.println( getName() + " deals " + y + " points of damage.");
        return y;
    }

    //with this, the user has a 50/50 chance to nulify all damage
    //otherwise, it'll just negate a certain amout of damage
    //e.g. damage reduced by 100 etc.
    public void defend(){
        if(!isDef()){
            _defending = true;
        }
    }

    public void setDefend(boolean x){
        _defending = x;
    }

    //returns whether or not character is defending
    public boolean isDef(){
        return _defending;
    }

    //self explanatory
    public boolean isDead(){
        return getHP() <= 0;
    }

    //this method deals damage to the user
    public void setHP(int dmg){
        _HP = _HP - dmg;
    }

    //character dies
    //will differ betweeen player and enemies
    public void death(){

    }

    //returns how much hp has
    public int getHP(){
        return _HP;
    }

    public int getDefense(){
        return defense;
    }

}



#Control Spectre: Noire
by Ivy Wong, Vincent Ou, and Jim Yuan

A terminal-based choose-your-own-adventure game, where you play as James Noire, private investigator and Spectre hunter.

###Intro: 
You are about to enter the vast and diverse world that is 20th century Grand Shining. Your name is Noire, James Noire. You are part of a covert group of detectives, Control Spectre. It is our job to ensure the safety and security of GS citizens, no matter the cost. You, my young prodigy, are assigned this particular case regarding the hauntings at the Ershin Mansion. Time is of the essence. Get to it.

###To run (Windows, Mac, and Linux):
- Clone the repo into the directory of your choosing.
- To compile, use `javac CSN.java`.
- To run, type in `java CSN`.

//Control Spectre: Noire
//Main Character: Noire
//Driver file

import java.io.*;
import java.util.*;

public class CSN{

    private static Character noire;
    private static Character monster;
    private static InputStreamReader isr;
    private static BufferedReader br;


    //May use this later for pacing
    public static void pause(){
        try{
            Thread.sleep(5000);
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    public static int battle(){
        monster = new Monster();
        int input = 0;
        int d1 = 0;
        int d2 = 0;
        while( !monster.isDead() && !noire.isDead()) {
            try {
                String prompt;
                prompt = "\t1: Attack.\n\t2: Defend.\n";
                System.out.println( "Your HP: " + noire.getHP()+ "\nWhat is your decision?" );
                System.out.println(prompt);
                br = new BufferedReader(new InputStreamReader(System.in));
                input = Integer.parseInt( br.readLine() );
            }
            catch ( IOException e ) { }

            if ( input == 1 ){
                noire.attack(monster);
            }
            else if ( input == 2 ) {
                noire.defend();
            }
            else {
                System.out.println("INVALID INPUT"); //TODO: add loop here
            }
            if(!monster.isDead()){
                System.out.println("The Spectre attacks!");
                monster.attack( noire );
            }
        }
        if ( monster.isDead() && noire.isDead() ) {
            System.out.println( "You strike each other down.\n\n");
            return 1;
        }
        else if ( monster.isDead() ) {
            System.out.println( "Noire fires his gun and kills the Spectre.\n\n" );
            return 2;
        }
        else if ( noire.isDead() ) {
            System.out.println( "The spectre strikes you down.\n\n" );
            return 1;
        } else {
            return 2;
        }
    }

    public static void setupStory( Story story, String indexFile ){ 
        String indexPath = System.getProperty("user.dir") + indexFile;
        try{
            //Add all the scenes to story tree
            Scanner sc = new Scanner(new File(indexPath));
            while(sc.hasNextLine()){
                boolean battle = false, end = false;
                // index, prompt, filename, isBattle()
                String[] data = sc.nextLine().split("=");
                Story sub = new Story();
                //System.out.println(data[0]);
                if( data[3].equals("true")){
                    battle = true;
                } else if(data.length > 4){
                    //System.out.println("here be pirates");
                    if( !data[4].equals("END")){
                        //System.out.println("SUB");
                        setupStory(sub, "/scenes/" + data[4]);
                    } else {
                        end = true;
                    }
                }
                if(sub.isEmpty()){
                    sub = null;
                }
                story.add( Integer.parseInt(data[0]), new Scene(data[1], data[2], battle, sub, end));
                //System.out.println("Read scene.");
            }
        }
        catch( FileNotFoundException e ){
            System.out.println("File not found.");
        }
    }

    public static void traverse(Story story){
        //Start game
        boolean firstInit = true;
        Scanner sc;
        while( !story.getCurrent().isEnding()){
            if(story.getCurrent().hasBattle()){
                //System.out.println("BATTTTLE HEREEEEE");
                System.out.println("Battle commence:");
                int result = battle();
                if(result == 1){ //death
                    story.goLeft();
                } if(result == 2){ //continue
                    story.goRight();
                }
            } else {
                if(firstInit){
                    firstInit = false;
                    System.out.println(story.getCurrent().getContent());
                }
                System.out.println(story.getCurrent().getPrompt());   

                    sc = new Scanner(System.in);
                String input = sc.next();
                if(input.equals("1")){
                    story.goLeft();
                } else if(input.equals("2")){
                    story.goRight();
                } else {
                    System.out.println("Invalid answer.");
                }
            }
            System.out.println("\n-------------------------------------------------\n");
            //System.out.println("Current: " + story.getCurrentIndex());
            if(story.getCurrent().hasSubtree()){
                //System.out.println("SUBBBB");
                traverse(story.getCurrent().getSubtree());
            }

            System.out.println("\n" + story.getCurrent().getContent());;
        }
    }

    public static void main(String [] args){
        //create characters
        noire = new Hunter();
        System.out.println( "[2J" );
        System.out.println(
                "You are about to enter the vast and diverse world that is 20th century Grand Shining. Your name is Noire, James Noire. You are part of a covert group of detectives, Control Spectre. It is our job to ensure the safety and security of GS citizens, no matter the cost. You, my young prodigy, are assigned this particular case regarding the hauntings at the Ershin Mansion. Time is of the essence. Get to it.\n");
        Console c = System.console();
        if (c != null) {
            c.format("\nPress ENTER to proceed.\n");
            c.readLine();
        } 
        System.out.println(
                "...............*........................*............_.---._.........................\n"+
                ".............................___................___..............*...................\n"+
                "......................._____[LLL]______________[LLL]_____.....)......................\n"+
                "......................(.....[LLL]..............[LLL].....).....|.....................\n"+
                ".....................(____________________________________)....|.....................\n"+
                "......................)==================================(....(......................\n"+
                "............*..........|I..-..I..-..I..--..I..-..I..-..I|............................\n"+
                "..................*....|I.|+|.I.|+|.I.|..|.I.|+|.I.|+|.I|-.`.......*.................\n"+
                ".......................|I_|+|_I_|+|_I_|__|_I_|+|_I_|+|_I|............................\n"+
                "......................(_I_____I_____I______I_____I_____I_)...........................\n"+
                ".......................)================================(...*........................\n"+
                ".......*........._.....|I..-..I..-..I..--..I..-..I..-..I|..........*.................\n"+
                "................|u|..__|I.|+|.I.|+|.I.|<>|.I.|+|.I.|+|.I|...._.......................\n"+
                "...........__...|u|_|uu|I.|+|.I.|+|.I.|~.|.I.|+|.I.|+|.I|._.|U|....._................\n"+
                "..........|uu|__|u|u|u,|I_|+|_I_|+|_I_|__|_I_|+|_I_|+|_I||n||.|____|u|...............\n"+
                "..........|uu|uu|_,.-..(I_____I_____I______I_____I_____I)`.-..|uu.u|u|__.............\n"+
                "..........|uu.-.`......#############(______)#############....`.-..u|u|uu|............\n"+
                "........._..`..............~..~...(________)...~..^~...........`.-.|uu|..............\n"+
                "......,.................._............................._.`.-.........`.-.............\n"+
                "..~.^.~...._,.~.^.~...._(.)_........................._(.)_...`.-.........~.^.~.......\n"+
                "......_................|___|.........................|___|......~.^.~....._..........\n"+
                "...._(.)_..............|_|_|..........().()..........|_|_|.............._(.)_........\n"+
                "....|___|()()()()()()()|___|()()()()()||.||()()()()()|___|()()()()()()()|___|........\n"+
                "....|_|_|)()()()()()()(|_|_|)()()()()(||.||)()()()()(|_|_|)()()()()()()(|_|_|........\n"+
                "....|___|()()()()()()()|___|()()()()()||.||()()()()()|___|()()()()()()()|___|........\n"+
                "....|_|_|)()()()()()()(|_|_|)()()()()([===])()()()()(|_|_|)()()()()()()(|_|_|........\n"+
                "....|___|()()()()()()()|___|()()()()()||.||()()()()()|___|()()()()()()()|___|........\n"+
                "....|_|_|)()()()()()()(|_|_|)()()()()(||.||)()()()()(|_|_|)()()()()()()(|_|_|........\n"+
                "....|___|()()()()()()()|___|()()()()()||.||()()()()()|___|()()()()()()()|___|........\n"+
                "~..~|_|_|)()()()()()()(|_|_|)()()()()(||.||)()()()()(|_|_|)()()()()()()(|_lc|~..~....\n"+
                "...[_____]............[_____].......................[_____]............[_____].......\n"+
                ".....................................................................................\n"+
                ".....................................................................................\n"+
                "..................SPECTRES SIGHTED IN ERSHIN MANSION. REQUESTING ASSISTANCE..........\n"+
                ".....................................................................................\n");

        //Allow user to read intro before continuing
        Console x = System.console();
        if (x != null) {
            x.format("\nPress ENTER to proceed.\n");
            x.readLine();
        } 

        //START GAME 
        Story story = new Story(); 
        setupStory(story, "/scenes/index.dat"); 
        traverse(story);
    }
}

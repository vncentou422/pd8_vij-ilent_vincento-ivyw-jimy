import java.util.*;
import java.io.*;

public class Monster extends Character{
    public Monster(){
        super();
        name = "The Spectre";
        _HP = 50;
        attack = 10;
        defense = 5;
    }
}

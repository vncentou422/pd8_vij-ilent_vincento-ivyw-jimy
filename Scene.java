import java.io.*;
import java.util.*;

public class Scene {

    private String prompt, filename; 
    private boolean hasBattle, isEnding;
    private Story _subtree;

    public Scene( String p, String file, boolean battle, Story sub, boolean end ){
        prompt = p;
        filename = file;
        hasBattle = battle;
        _subtree = sub;
        isEnding = end;
    }

    public boolean hasSubtree(){
        return _subtree != null;
    }
    public Story getSubtree(){
        return _subtree;
    }
    public void addSubtree(Story s){
        _subtree = s;
    }

    public String getPrompt(){
        return prompt;
    }

    public String getFileName(){
        return filename;
    }

    public String getContent(){
        String content = "";
        try {
            String path = System.getProperty("user.dir") + "/scenes/" + filename;
            Scanner sc = new Scanner(new File(path));
            while(sc.hasNextLine()){
                content += sc.nextLine() + "\n";
            }
        } catch (FileNotFoundException e){
            System.out.println("File not found.");
        }
        return content;
    }

    public boolean hasBattle(){
        return hasBattle;
    }

    public boolean isEnding(){
        return isEnding;
    }

    public String toString(){
        return prompt;
    }

}

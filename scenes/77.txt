You take aim and shoot at the Spectre. The bullet sinks in, but the Spectre isn't even fazed. It turns to you, its eyes flaring to red. It should have died, you think, and reload and fire again. Nothing.

Your eyes flicker to the figure on the ground. You're almost certain it's O'Connor, and he doesn't seem to be breathing.

Damn Spectre, you think. You fire until you're out of bullets, and then turn to charms when you run out. Nothing. The Spectre's coming closer - you back away, but you bump into something.

You turn, and find yourself facing a woman. Her eyes are glowing red.

She raises a hand, clawed and skinless and ugly, and plunges it into your heart.

The Spectre goes down to a neatly placed knife and a bullet to the head. 

You pause to catch your breath, and then continue down the hall. 

As you reach the stairs, someone rushes out in front of you and you nearly fire a round at - a woman?

She skids to a stop, all blue cloth and flyaway red hair. "I - you're not supposed to be here," she whispers. Her voice sounds vaguely familiar. She shakes her head and says more loudly, "Follow me."

You exchange a look with O'Connor. He furrows his brow and stares at the woman intently.

She leads you two down the stairs and out the main entrance. Strangely, there is no sign of any Spectre activity as she does so. You reach the front gate without any trouble.

The night is still dark as ever, and the moon is shining brightly amidst all the stars. The woman looks up at them for a moment before turning back to you.

"Please don't come here again," she says. "It's- not safe." Her voice starts trembling again, but her gaze holds strong. 

You're inclined to agree. "We won't," you say. "Let's get you to the doctor," you say to O'Connor, who looks exhausted. He nods faintly. You help him down the street, and don't look back.

NORMAL END

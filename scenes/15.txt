You force O'Connor to sit down on a nearby chair so you can grab the roll of bandages out of your bag and wrap the wound. He stays still reluctantly, and idly flicks a switchblade open and closed as you do so.

When you finish, he stands up with a wince. "Let's go," he says. "We've wasted enough time already."

He leans on you for support, and the two of you move down the hall carefully. As you reach the stairs, someone rushes out in front of you and you nearly fire a round at - a woman?

She skids to a stop, all blue cloth and flyaway red hair. "I - you're not supposed to be here," she whispers. Her voice sounds vaguely familiar.

O'Connor tugs at your sleeve and grunts, "No time to deal with this. We gotta go *now*."

You look at the woman. She's wringing her hands and looking around frantically.

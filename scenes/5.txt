"Thank you, I guess I will. It'll take a while to explain to you what I know, anyway," She says. 

She hangs her jacket on the rack and seats herself on the other side of your desk. You seat yourself in the familiar soft blue cushy chair at your desk.

"But before all that, I must know your name. After all, you are my client and you are going to be hiring me," you say.

"Oh dear, I'm sorry. My name is Jannette Wilson, but please, call me Jenny," she begins.

You cut her off, asking her to explain what she wanted to say earlier in the hall. 

"I'm hearing strange noises at night, and I can't sleep because of them," she says. You take notice of very light bags under her eyes. "I feel like there's someone watching me, and I think they've broken into my house. I've tried the police before, but the call cuts off right before I'm connected. I thought it odd, but kept trying, but now I can't, because I'm so scared."

You want to console her, but you don't know if you should.

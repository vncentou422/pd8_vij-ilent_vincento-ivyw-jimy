You grab your car keys and coat and help Jenny into her coat before the two of you make your way to Jenny's house. 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the parking lot, you get into your car and put the key in the ignition. The engine turns over and your Blue Charger roars. You back out of the parking lot and drive to Jenny's house.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You pull over in front of Jenny's house. The only light nearby is a flickering street lamp that suddenly goes out. You ignore it as Jenny opens the door to her house and you hear a banging sound coming from under the stairs.

You pull out your gun, ready in case someone was there. You slowly approach the stairs.
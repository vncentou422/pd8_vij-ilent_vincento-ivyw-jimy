import java.util.ArrayList;

public class Story {
   
    //instance vars
    private ArrayList<Scene> _tree; //underlying container
    private int _currentScene;

    /*****************************************************
     * default constructor  ---  inits empty tree
     *****************************************************/
    public Story() {
        _tree = new ArrayList<Scene>(); 
        _currentScene = 0;
    }//O(1)

    /*****************************************************
     * boolean isEmpty()
     * Returns true if no meaningful elements in tree, false otherwise
     *****************************************************/
    public boolean isEmpty(){
        return _tree.size() == 0; 
    }//O(1)

    public void add( int pos, Scene s ){
        _tree.add( pos, s );
    }
   
    public void addLeft( int parentPos, Scene addVal ) {
        _tree.add( parentPos*2 + 1, addVal );    
    }
    
    public void addRight( int parentPos, Scene addVal ) {
        _tree.add( parentPos*2 + 2, addVal );    
    }

    public String toString(){
        return _tree.toString();
    }//O(n)

    public void goLeft(){
        _currentScene = _currentScene*2+1;
    }

    public void goRight(){
        _currentScene = _currentScene*2+2;
    }

    public Scene getCurrent(){
        return _tree.get(_currentScene);
    }

    public int getCurrentIndex(){
        return _currentScene;
    }

    public int size(){
        return _tree.size();
    }

    //returns true if there are no more choices
    public boolean reachedEnd(){
        int left = _currentScene*2+1;
        int right = _currentScene*2+2;
        return left >= _tree.size() && right >= _tree.size();
    } 
}
